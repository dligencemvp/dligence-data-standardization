import sys
import json
import base64
import googlemaps
import pandas as pd
gmaps = googlemaps.Client(key='AIzaSyB95JdjZ-f2bjkz10afSY810gO3g-pSOpI') # This needs to be externalized. This is my personal account

def format_address(data):
    _entity_info_frame = pd.json_normalize(data['body']['entity_info'])
    _entity_info_frame['Formatted Address'] = ''
    for i in range(len(_entity_info_frame)):
        _entity_info_frame['Formatted Address'].iat[i] = gmaps.geocode(
            _entity_info_frame['Main Office Address'].iat[i])
    result = _entity_info_frame.to_json(orient='records')
    parsed = json.loads(result)
    data['body']['entity_info'] = parsed
    print(data['body'])
    return data['body']

if __name__ == '__main__':
    for i in range(len(sys.argv)):
        if(i==1):
            print(sys.argv[i])
            data = json.loads(sys.argv[i])
            print(data)
            standardizationOutput = format_address(data)
            with open('/tmp/standardization.json', 'w') as f:
                json.dump(standardizationOutput, f, ensure_ascii=False, indent=4)





